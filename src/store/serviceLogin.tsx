import Axios from "axios";

const POCETNA_ADRESA = "http://localhost/";

export const login = (a: any) => Axios.post(POCETNA_ADRESA+'evidencija/backEnd/api/loginTajnica.php',a)
    .then(function(res){
      if(res.status===200){
        sessionStorage.setItem("userInfo", JSON.stringify(res.data.jwt))
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });


