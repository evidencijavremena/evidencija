import Axios from "axios";
import {Car} from "../components/Cars";

const POCETNA_ADRESA = "http://localhost";

export const addCar = (car: Car) =>
    Axios.post(POCETNA_ADRESA + '/evidencija/backEnd/api/dodajAutomobil.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!), ...car})
        .then(function (res) {
          console.log("bla");
          if (res.status === 200) {
            return res;
          }
        })
        .catch(function (error) {
          console.log(error);
          return error;
        });


export const getCars = () => Axios.post(POCETNA_ADRESA+'/evidencija/backEnd/api/ispisiAutomobile.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const deleteCar = (a: any) => Axios.delete(POCETNA_ADRESA+'/evidencija/backEnd/api/izbrisiAutomobil.php', {data: {...a, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)}})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const updateCar = (car: Car) => Axios.put(POCETNA_ADRESA+'/evidencija/backEnd/api/azurirajAutomobil.php', {...car, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });
