import {Uredaj} from "../components/Device";
import Axios from "axios";

const POCETNA_ADRESA = "http://localhost";

export const addDevice = (device: Uredaj) =>
    Axios.post(POCETNA_ADRESA + '/evidencija/backEnd/api/dodajUredaj.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!), ...device})
        .then(function (res) {
          console.log("bla");
          if (res.status === 200) {
            return res;
          }
        })
        .catch(function (error) {
          console.log(error);
          return error;
        });


export const getDevices = () => Axios.post(POCETNA_ADRESA+'/evidencija/backEnd/api/ispisiUredaje.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const deleteDevice = (a: any) => Axios.delete(POCETNA_ADRESA+'/evidencija/backEnd/api/izbrisiUredaj.php', {data: {...a, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)}})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const updateDevice = (device: Uredaj) => Axios.put(POCETNA_ADRESA+'/evidencija/backEnd/api/azurirajUredaj.php', {...device, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });
