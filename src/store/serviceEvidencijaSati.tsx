import Axios from "axios";
import {OcitanjeKorisnika} from "../components/evidencijaOcitanja";
import { PodaciZaRadnoVrijeme } from "../components/evidencijaSati";

const POCETNA_ADRESA = "http://localhost/";

export const getRegisteredReading = () => Axios.post(POCETNA_ADRESA+'evidencija/backEnd/api/ispisiSvaOcitanja.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

 export const getRegisteredReadingInRange = (interval: OcitanjeKorisnika) =>
    Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/ispisiSvaOcitanjaUIntervalu.php", {...interval, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const izracunajRadnoVrijemeZaKorisnikaUIntervalu = (podaciZaRadnoVrijeme: PodaciZaRadnoVrijeme) =>
    Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/izracunajRadnoVrijemeZaKorisnikaUIntervalu.php", {...podaciZaRadnoVrijeme, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const izracunajRadnoVrijemeZaSveKorisnikeUIntervalu = (datumi: any) =>
    Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/izracunajRadnoVrijemeZaSveKorisnikeUIntervalu.php", {...datumi, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
        .then(function(res){
          if(res.status===200){
            return res;
          }
        })
        .catch(function(error){
          console.log(error);
          return error;
        });

export const ispisiSvaOcitanjaZaKorisnika = (idUsporedba: OcitanjeKorisnika) =>
    Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/ispisiSvaOcitanjaZaKorisnika.php", {...idUsporedba, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });


export const ispisiSvaOcitanjaZaKorisnikaUIntervalu = (idUsporedbaInterval: OcitanjeKorisnika) =>
    Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/ispisiSvaOcitanjaZaKorisnikaUIntervalu.php", {...idUsporedbaInterval, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

