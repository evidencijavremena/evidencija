import Axios from "axios";
import {User} from "../components/osobe";

const POCETNA_ADRESA = "http://localhost/";

export const addUser = (user: User) => Axios.post(POCETNA_ADRESA+"evidencija/backEnd/api/dodajKorisnika.php",{...user, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const getUsers = () => Axios.post(POCETNA_ADRESA+'evidencija/backEnd/api/ispisiKorisnike.php', {jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const deleteUser = (a: any) => Axios.delete(POCETNA_ADRESA+"evidencija/backEnd/api/izbrisiKorisnika.php", {data: {...a, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)}})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });

export const updateUser = (user: User) => Axios.put(POCETNA_ADRESA+'evidencija/backEnd/api/azurirajKorisnika.php ', {...user, jwt: JSON.parse(sessionStorage.getItem("userInfo")!)})
    .then(function(res){
      if(res.status===200){
        return res;
      }
    })
    .catch(function(error){
      console.log(error);
      return error;
    });
