import React, {Component} from "react";
import {Alert, Button, Form, Modal, Table} from "react-bootstrap";
import {addCar, deleteCar, getCars, updateCar} from "../store/serviceAutomobil";

export interface Car{
  ID: number;
  naziv: string;
}

export class Cars extends Component {
  state = {
    showModal: false,
    listOfCars: [] as Car[],
    naziv: "",
    naslov: "Dodaj automobil",
    pocetniCar: 0,
    id: -1,
    nemaNaziva: false
  }

  private brojac = 0;
  private jeliNovi = true;
  private idUpdate = -1;

  componentDidMount(): void {
    console.log()
    getCars().then(res => {
      if(res.status === 200){
        console.log(res.data);
        const {data} = res.data;
        if(data){
          this.setState({listOfCars: data});
        }
        else {
          this.setState({listOfCars: []});
        }
      }else{
        console.log("ne radi get osoba");
      }}
    ).catch(error => {
      console.log(error);
    })
  }

  render() {
    const showModalHandler = () => {
      this.setState({showModal: true});
    }

    const closeModalHandler = () => {
      this.setState({showModal: false,
        naziv: "",
        naslov: "Dodaj automobil",
        id: -1,
        nemaNaziva: false});
    }

    const deletingRow = (id: number) => {
      const noviCars = this.state.listOfCars.filter(car => (car as Car).ID!==id);
      deleteCar({ID: id})
          .then((res) => {
                if(res.status===200){
                  this.setState({listOfCars: noviCars});
                }
                else{
                  console.log("nije se obrisao uredaj");
                }
              }

          ).catch((error) => {
        console.log(error);
      })
    }

    const urediRow = (id: number) => {
      this.jeliNovi = false;
      this.idUpdate = id;
      let cars;
      const b = this.state.listOfCars.find(car => (car as Car).ID === id);
      if(b){
        cars = b as Car;
      }
      else{
        cars = {naziv: "",ID: id} as Car;
      }
      this.setState({showModal: true,
        naziv: cars.naziv,
        naslov: "Uredi automobil"});
    }

    const saveCar = () => {
      const noviCar = {
        naziv: this.state.naziv
      } as Car;
      let carovi: Car[] = [];
      if(this.state.naziv) {
        if (this.jeliNovi) {
          carovi = this.state.listOfCars;
          addCar(noviCar).then((res) => {
            console.log("je li se 2 puta izvrsi");
            if (res.status === 200) {
              console.log(carovi);
              console.log(noviCar);
              console.log(this.state);
              this.setState({
                listOfCars: [...carovi, noviCar],
                showModal: false,
                naziv: "",
                naslov: "Dodaj automobil",
                nemaNaziva: false
              });
            } else {
              console.log("desila se greska");
            }
          }).catch(error => {
            console.log(error);
          })
        } else {
          const stariCarovi = this.state.listOfCars.filter(car => (car as Car).ID !== this.idUpdate);
          noviCar.ID = this.idUpdate;
          updateCar(noviCar).then((res) => {
            if (res.status === 200) {
              this.jeliNovi = true;
              this.idUpdate = -1;
              this.setState({
                listOfCars: [...stariCarovi, noviCar],
                showModal: false,
                naziv: "",
                naslov: "Dodaj automobil",
                nemaNaziva: false
              });
            } else {
              console.log("desila se greska");
            }
          }).catch(error => {
            console.log(error);
          })
        }
      }
      else{
        this.setState({nemaNaziva: true});
      }
    }

    const returnTabledCarovi = () => {
      return sortCarovi(this.state.listOfCars).slice(this.state.pocetniCar,this.state.pocetniCar+10).map((car) => {
        return <tr>
          <td>{car.naziv}</td>
          <td><Button onClick={() => urediRow(car.ID)}>uredi</Button></td>
          <td><Button onClick={() => deletingRow(car.ID)}>izbriši</Button></td>
        </tr>
      })
    }

    const sortCarovi = ( carovi: Car[] ) => {
      const sortiranicarovi = [...carovi];
      return sortiranicarovi.sort((a,b) => a.ID - b.ID);
    }

    const naprijed = () => {
      if (this.state.pocetniCar + 10 < this.state.listOfCars.length) {
        this.setState({pocetniCar: this.state.pocetniCar + 10})
      }
    }

    const nazad = () => {
      if (this.state.pocetniCar > 0) {
        this.setState({pocetniCar: this.state.pocetniCar - 10})
      }
    }



    return(
        <div>
          <Button style={{marginTop: "20px", marginLeft: "20px"}} variant="info" onClick={showModalHandler}>Dodaj automobil</Button>
          <Modal show={this.state.showModal} onHide={closeModalHandler}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.naslov}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="naziv">
                  <Form.Label>Naziv automobila</Form.Label>
                  <Form.Control type="text" value={this.state.naziv} onChange={this.settingCar}/>
                </Form.Group>
              </Form>
              {this.state.nemaNaziva?
                  <Alert variant={"warning"}>
                    Niste upisali naziv automobila
                  </Alert>: null}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeModalHandler}>
                Close
              </Button>
              <Button variant="primary" onClick={saveCar}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
          <div className="container">
            <Table className="table">
              <thead>
              <tr>
                <th>naziv automobila</th>
                <th>uredi</th>
                <th>izbriši</th>
              </tr>
              </thead>
              <tbody>
              {returnTabledCarovi()}
              </tbody>
            </Table>
            <div className="bg-info">
              <Button variant="primary" className="btn btn-secondary float-left" onClick = {nazad} children={"<<"}/>
              <Button variant="primary" className="btn btn-secondary float-right" onClick = {naprijed} children={">>"}/>
            </div>
          </div>
        </div>
    );
  }

  private settingCar = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, naziv: text})
  }

  private settingLokaciju = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, lokacija: text})
  }
}
