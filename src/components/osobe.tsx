import React, {Component} from "react";
import {Alert, Button, Form, FormControl, InputGroup, Modal} from "react-bootstrap";
import {addUser, deleteUser, getUsers, updateUser} from "../store/serviceZaposlenik";

export interface User{
  ime: string,
  prezime: string,
  datumRodenja: string,
  pozicija: string,
  ID: number
}

export class Osobe extends Component {
  private dateReg = /^\d{1,2}([./-])\d{1,2}\1\d{4}$/;
  private jeliNovi = true;
  private idUpdate = -1;

  state = {
    user: "",
    users: [],
    person: {},
    showModal: false,
    ime: "",
    prezime: "",
    datumRodenja: "",
    pozicija: "Zaposlenik",
    dateError: false,
    id: -1,
    kadrovskiBroj: -1,
    naslov: "Dodaj zaposlenika",
    pocetniZaposlenikPregled: 0,
    pocetniZaposlenikPregledFiltrirani: 0,
    errorIme: false,
    errorPrezime: false
  }

  public sortUsers( users: User[] ){
    const sortiraniUseri = [...users];
    return sortiraniUseri.sort((a,b) => a.ID - b.ID);
  }

  componentDidMount(): void {
    console.log(JSON.parse(sessionStorage.getItem("userInfo")!));
    getUsers().then(res => {
      if(res.status === 200){
        console.log(res.data);
        const {data} = res.data;
        if(data) {
          this.setState({users: data});
        }else{
          this.setState({users: []});
        }
      }else{
        console.log("ne radi get osoba");
      }}
    ).catch(error => {
      console.log(error);
    })
  }


  render() {
    console.log(this.state.users);
    const showModalHandler = () => {
      this.setState({showModal: true});
    }

    const closeModalHandler = () => {
      this.setState({showModal: false,
        ime: "",
        prezime: "",
        datumRodenja: "",
        pozicija: "Zaposlenik",
        id: -1,
        naslov: "Dodaj zaposlenika",
        errorIme: false,
        errorPrezime: false
      });
    }

    const deletingRow = (id: number) => {
      const noviUseri = this.state.users.filter(luser => (luser as User).ID!==id);
      deleteUser({ID: id})
          .then((res) => {
                if(res.status===200){
                  this.setState({users: noviUseri});
                }
                else{
                  console.log("nije se obrisao osoba");
                }
              }

          ).catch((error) => {
        console.log(error);
      })
    }

    const urediRow = (id: number) => {
      this.jeliNovi = false;
      this.idUpdate = id;
      let osoba;
          const b = this.state.users.find(user => (user as User).ID === id);
      if(b){
        osoba = b as User;
      }
      else{
        osoba = {ime: "",
          prezime: "",
          datumRodenja: "",
          pozicija: "",
          ID: id} as User;
      }
      this.setState({showModal: true,
        ime: osoba.ime,
        prezime: osoba.prezime,
        datumRodenja: osoba.datumRodenja,
        pozicija: osoba.pozicija,
        naslov: "Uredi zaposlenika"});
    }

    const saveUser = () => {
      if(!this.state.ime || !this.state.prezime){
        if(this.state.ime){
          this.setState({errorIme: false});
        }
        else{
          this.setState({errorIme: true});
        }
        if(this.state.prezime){
          this.setState({errorPrezime: false});
        }
        else{
          this.setState({errorPrezime: true});
        }
      }
      else {
        const noviUser = {
          ime: this.state.ime,
          prezime: this.state.prezime,
          datumRodenja: this.state.datumRodenja,
          pozicija: this.state.pozicija
        } as User;
        let useri: User[] = [];

        if(this.jeliNovi) {
          useri = this.state.users;
          if(noviUser.ime && noviUser.prezime) {
            addUser(noviUser).then((res) => {
              if (res.status === 200) {
                noviUser.ID=res.data.id;
                this.setState({
                  users: [...useri, noviUser],
                  showModal: false,
                  ime: "",
                  prezime: "",
                  datumRodenja: "",
                  pozicija: "Zaposlenik",
                  dateError: false,
                  errorIme: false,
                  errorPrezime: false
                });
              } else {
                console.log("desila se greska");
              }
            }).catch(error => {
              console.log(error);
            })
          }
        }else{
          const stariUseri = this.state.users.filter(user => (user as User).ID !== this.idUpdate);
          noviUser.ID = this.idUpdate;
          updateUser(noviUser).then((res) => {
            if(res.status===200){
              this.jeliNovi = true;
              this.idUpdate = -1;
              this.setState({
                users: [...stariUseri, noviUser],
                showModal: false,
                ime: "",
                prezime: "",
                datumRodenja: "",
                pozicija: "Zaposlenik",
                dateError: false,
                naslov: "Dodaj zaposlenika",
                errorIme: false,
                errorPrezime: false
              });
            }
            else{
              console.log("desila se greska");
            }
          }).catch(error => {
            console.log(error);
          })
        }
      }
    }

    const filterPeople = () => {
      const username = this.state.user.trim().replace( /\s\s+/g, " ");
      if(username && this.state.users) {
        return (this.state.users as User[])
            .filter(user => (user as User).ime.startsWith(username) ||
                (user as User).prezime.startsWith(username) ||
                ((user as User).ime + ' ' + (user as User).prezime).startsWith(username) ||
                ((user as User).prezime + ' ' + (user as User).ime).startsWith(username));
      }
      return this.state.users;
    }

    const showPeople = () => {
        const filtriraneOsobe = filterPeople();
        if(this.state.user && this.state.users){
          const poljeOsoba = this.sortUsers(filtriraneOsobe).slice(this.state.pocetniZaposlenikPregledFiltrirani,this.state.pocetniZaposlenikPregledFiltrirani+10).map(user => vratiRedak(user));
          return poljeOsoba;
        }
        const poljeOsoba = this.sortUsers(this.state.users).slice(this.state.pocetniZaposlenikPregled,this.state.pocetniZaposlenikPregled+10).map(user => vratiRedak(user));
        return poljeOsoba;
    }

    const vratiRedak = (user: User) => {
      return <tr>
          <th scope="row">{user.ID}</th>
          <td>{user.ime}</td>
          <td>{user.prezime}</td>
          <td>{user.pozicija}</td>
          <td><Button onClick={() => urediRow(user.ID)}>uredi</Button></td>
          <td><Button onClick={() => deletingRow(user.ID)}>izbriši</Button></td>
        </tr>
    }

    const naprijed = () => {
      if (this.state.user) {
        if (this.state.pocetniZaposlenikPregledFiltrirani + 10 < filterPeople().length) {
          this.setState({pocetniZaposlenikPregledFiltrirani: this.state.pocetniZaposlenikPregledFiltrirani + 10})
        }
      }
      else{
        if (this.state.pocetniZaposlenikPregled + 10 < this.state.users.length) {
          this.setState({pocetniZaposlenikPregled: this.state.pocetniZaposlenikPregled + 10})
        }
      }
    }

    const nazad = () => {
      if (this.state.user) {
        if (this.state.pocetniZaposlenikPregledFiltrirani > 0) {
          this.setState({pocetniZaposlenikPregledFiltrirani: this.state.pocetniZaposlenikPregledFiltrirani - 10})
        }
      }
      else{
        if (this.state.pocetniZaposlenikPregled > 0) {
          this.setState({pocetniZaposlenikPregled: this.state.pocetniZaposlenikPregled - 10})
        }
      }
    }

    return (
        <div>
        <InputGroup style={{width: "40%", marginTop: "20px", marginLeft: "420px"}}>
            <FormControl
                placeholder="Ime i prezime"
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
                onChange={(event: any) => {
                  if (event.target.value !== "") {
                    this.setState({user: event.target.value, pocetniZaposlenikPregled: 0 });
                  } else {
                    this.setState({user: event.target.value, pocetniZaposlenikPregledFiltrirani: 0 });
                  }
                }}
                value={this.state.user}
            />

        </InputGroup>

          <Button style={{marginTop: "10px", marginLeft: "20px"}} variant="info" onClick={showModalHandler}>Dodaj zaposlenika</Button>
          <Modal show={this.state.showModal} onHide={closeModalHandler}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.naslov}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="ime">
                  <Form.Label>Ime</Form.Label>
                  <Form.Control type="text" value={this.state.ime} onChange={(event: any) => this.setState({ime: event.target.value})}/>
                </Form.Group>
                {this.state.errorIme?
                    <Alert variant={"warning"}>
                      Niste upisali ime
                    </Alert>: null}
                <Form.Group controlId="prezime">
                  <Form.Label>Prezime</Form.Label>
                  <Form.Control type="text" value={this.state.prezime} onChange={(event: any) => this.setState({prezime: event.target.value})}/>
                </Form.Group>
                {this.state.errorPrezime?
                    <Alert variant={"warning"}>
                      Niste upisali prezime
                    </Alert>: null}
                <Form.Group controlId="formGridState">
                  <Form.Label>Pozicija</Form.Label>
                  <Form.Control as="select" defaultValue = {this.state.pozicija} onChange = {(event: any) => this.setState({pozicija: event.target.value})}>
                    <option>Zaposlenik</option>
                    <option>Volonter</option>
                  </Form.Control>
                </Form.Group>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={closeModalHandler}>
                Close
              </Button>
              <Button variant="primary" onClick={saveUser}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>
        <div className="container" >
        <table className="table">
          <thead>
          <tr>
            <th scope="col">Kadrovski Broj</th>
            <th scope="col">Ime</th>
            <th scope="col">Prezime</th>
            <th scope="col">radnoMjesto</th>
            <th scope="col">uredi</th>
            <th scope="col">izbriši</th>
          </tr>
          </thead>
          <tbody>
            {showPeople()}
          </tbody>
        </table>
        <div className="bg-info">
          <Button variant="primary" className="btn btn-secondary float-left" onClick = {nazad} children={"<<"}/>
          <Button variant="primary" className="btn btn-secondary float-right" onClick = {naprijed} children={">>"}/>
        </div>
        </div>
        </div>
    );
  }

}
