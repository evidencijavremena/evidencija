import React, {Component} from "react";
import {Alert, Button, Form, Modal, Table} from "react-bootstrap";
import {addDevice, deleteDevice, getDevices, updateDevice} from "../store/serviceUredaj";

export interface Uredaj{
  ID: number;
  naziv: string;
  kod: string;
}

export class Device extends Component {
  state = {
    showModal: false,
    listOfDevices: [] as Uredaj[],
    naziv: "",
    kod: "",
    naslov: "Dodaj uredaj",
    pocetniUredajPregled: 0,
    id: -1,
    errorNaziv: false,
    errorKod: false
  }

  private brojac = 0;
  private jeliNovi = true;
  private idUpdate = -1;

  componentDidMount(): void {
    console.log()
    getDevices().then(res => {
      if(res.status === 200){
        console.log(res.data);
        const {data} = res.data;
        if(data){
          this.setState({listOfDevices: data});
        }
        else {
          this.setState({listOfDevices: []});
        }
      }else{
        console.log("ne radi get osoba");
      }}
    ).catch(error => {
      console.log(error);
    })
  }

  render() {
    const showModalHandler = () => {
      this.setState({showModal: true});
    }

    const closeModalHandler = () => {
      this.setState({showModal: false,
      naziv: "",
      naslov: "Dodaj uredaj",
      kod: "",
      id: -1,
      errorKod: false,
      errorNaziv: false});
    }

    const deletingRow = (id: number) => {
      const noviDevicovi = this.state.listOfDevices.filter(device => (device as Uredaj).ID!==id);
      deleteDevice({ID: id})
          .then((res) => {
                if(res.status===200){
                  this.setState({listOfDevices: noviDevicovi});
                }
                else{
                  console.log("nije se obrisao uredaj");
                }
              }

          ).catch((error) => {
        console.log(error);
      })
    }

    const urediRow = (id: number) => {
      this.jeliNovi = false;
      this.idUpdate = id;
      let device;
      const b = this.state.listOfDevices.find(device => (device as Uredaj).ID === id);
      if(b){
        device = b as Uredaj;
      }
      else{
        device = {naziv: "",kod: "",ID: id} as Uredaj;
      }
      this.setState({showModal: true,
        naziv: device.naziv,
        kod: device.kod,
        naslov: "Uredi uredaj"});
    }

    const saveDevice = () => {
      if(this.state.naziv && this.state.kod) {
        const noviDevice = {
          naziv: this.state.naziv,
          kod: this.state.kod
        } as Uredaj;
        let devicovi: Uredaj[] = [];

        if (this.jeliNovi) {
          devicovi = this.state.listOfDevices;
          addDevice(noviDevice).then((res) => {
            console.log("je li se 2 puta izvrsi");
            if (res.status === 200) {
              console.log(devicovi);
              console.log(noviDevice);
              console.log(this.state);
              this.setState({
                listOfDevices: [...devicovi, noviDevice],
                showModal: false,
                naziv: "",
                kod: "",
                naslov: "Dodaj uredaj",
                errorNaziv: false,
                errorKod: false
              });
            } else {
              console.log("desila se greska");
            }
          }).catch(error => {
            console.log(error);
          })
        } else {
          const stariDevicovi = this.state.listOfDevices.filter(device => (device as Uredaj).ID !== this.idUpdate);
          noviDevice.ID = this.idUpdate;
          updateDevice(noviDevice).then((res) => {
            if (res.status === 200) {
              this.jeliNovi = true;
              this.idUpdate = -1;
              this.setState({
                listOfDevices: [...stariDevicovi, noviDevice],
                showModal: false,
                naziv: "",
                kod: "",
                naslov: "Dodaj uredaj",
                errorNaziv: false,
                errorKod: false
              });
            } else {
              console.log("desila se greska");
            }
          }).catch(error => {
            console.log(error);
          })
        }
      }
      else{
        if(!this.state.naziv){
          this.setState({errorNaziv: true});
        }
        else{
          this.setState({errorNaziv: false});
        }
        if(!this.state.kod){
          this.setState({errorKod: true});
        }
        else{
          this.setState({errorKod: false});
        }
      }
    }

    const returnTabledDevices = () => {
      return sortUredaji(this.state.listOfDevices).slice(this.state.pocetniUredajPregled,this.state.pocetniUredajPregled+10).map((uredaj) => {
        return <tr>
          <td>{uredaj.naziv}</td>
          <td>{uredaj.kod}</td>
          <td><Button onClick={() => urediRow(uredaj.ID)}>uredi</Button></td>
          <td><Button onClick={() => deletingRow(uredaj.ID)}>izbriši</Button></td>
        </tr>
      })
    }

  const sortUredaji = ( uredaji: Uredaj[] ) => {
      const sortiraniUredaji = [...uredaji];
      return sortiraniUredaji.sort((a,b) => a.ID - b.ID);
    }

    const naprijed = () => {
        if (this.state.pocetniUredajPregled + 10 < this.state.listOfDevices.length) {
          this.setState({pocetniUredajPregled: this.state.pocetniUredajPregled + 10})
        }
    }

    const nazad = () => {
        if (this.state.pocetniUredajPregled > 0) {
          this.setState({pocetniUredajPregled: this.state.pocetniUredajPregled - 10})
        }
    }



    return(
        <div>
          <Button style={{marginTop: "20px", marginLeft: "20px"}} variant="info" onClick={showModalHandler}>Dodaj uredaj</Button>
          <Modal show={this.state.showModal} onHide={closeModalHandler}>
            <Modal.Header closeButton>
              <Modal.Title>{this.state.naslov}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group controlId="naziv">
                  <Form.Label>Naziv uređaja</Form.Label>
                  <Form.Control type="text" value={this.state.naziv} onChange={this.settingUredaj}/>
                </Form.Group>
                {this.state.errorNaziv?
                    <Alert variant={"warning"}>
                      Niste upisali naziv uređaja
                    </Alert>: null}
                <Form.Group controlId="naziv">
                  <Form.Label>Kod uredaja</Form.Label>
                  <Form.Control type="text" value={this.state.kod} onChange={this.settingKod}/>
                </Form.Group>
                {this.state.errorKod?
                    <Alert variant={"warning"}>
                      Niste upisali kod uređaja
                    </Alert>: null}
              </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={closeModalHandler}>
                  Close
                </Button>
                <Button variant="primary" onClick={saveDevice}>
                  Save Changes
                </Button>
            </Modal.Footer>
          </Modal>
          <div className="container">
            <Table className="table">
              <thead>
              <tr>
                <th>naziv uređaja</th>
                <th>kod</th>
                <th>uredi</th>
                <th>izbriši</th>
              </tr>
              </thead>
              <tbody>
                {returnTabledDevices()}
              </tbody>
            </Table>
            <div className="bg-info">
              <Button variant="primary" className="btn btn-secondary float-left" onClick = {nazad} children={"<<"}/>
              <Button variant="primary" className="btn btn-secondary float-right" onClick = {naprijed} children={">>"}/>
            </div>
          </div>
        </div>
    );
  }

  private settingUredaj = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, naziv: text})
  }

  private settingKod = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, kod: text})
  }

  private settingLokaciju = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, lokacija: text})
  }
}
