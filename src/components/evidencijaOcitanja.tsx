import React, {Component} from "react";
import {Button, Form, Row, Col, Alert, Modal} from "react-bootstrap";
import {getRegisteredReading, getRegisteredReadingInRange, ispisiSvaOcitanjaZaKorisnika, ispisiSvaOcitanjaZaKorisnikaUIntervalu} from "../store/serviceEvidencijaSati";
import '../pages/design/table.css';

export interface UserTime{
    ime: string,
    prezime: string,
    pozicija: string,
    vrijeme: string,
    razlog: string,
    lokacija: string,
    uredaj: string,
    automobil: string,
    ID: string
  }
export interface OcitanjeKorisnika{
    datumOd: string,
    datumDo: string,
    idKorisnika: string
}
export class EvidencijaOcitanja extends Component {
  
    state = {
      users: [{ID: "", ime: "", prezime: "", pozicija: "", vrijeme: "", razlog: "", lokacija: "", uredaj: "", automobil: ""}],
      fiksniusers: [{ID: "", ime: "", prezime: "", pozicija: "", vrijeme: "", razlog: "", lokacija: "", uredaj: "", automobil: ""}],
      noviusers: [{ID: "", ime: "", prezime: "", pozicija: "", vrijeme: "", razlog: "", lokacija: "", uredaj: "", automobil: ""}],
      showTwoDatesError: false,
      showDateOrderError: false,
      showDateErrorModal: false,
      imeUsporedba: "",
      prezimeUsporedba: "",
      korisnikOcitanja: {idKorisnika: "", datumOd: "", datumDo: ""} as OcitanjeKorisnika,
      prvoOcitanjeKojeSePrikazuje: 0
    }

    componentDidMount(): void {
      getRegisteredReading().then(res => {
        if(res.status === 200){
          console.log(res.data);
          const {data} = res.data;
          if(data){
            this.setState({users: data});
            this.setState({fiksniusers: data});
            this.setState({noviusers: data});
          }else{
            this.setState({users: []});
            this.setState({fiksniusers: []});
            this.setState({noviusers: []});
          }
        }else{
          console.log("ne radi get user vrijeme");
        }}
      ).catch(error => {
        console.log(error);
      })
    }

    componentDidUpdate(): void {
      if((this.state.korisnikOcitanja.idKorisnika !== "") && (this.state.korisnikOcitanja.datumOd !== "") && (this.state.korisnikOcitanja.datumDo !== "") &&
      (this.state.korisnikOcitanja.datumOd <= this.state.korisnikOcitanja.datumDo)){
        console.log("dohvat ocitanja po id i intervalu ", this.state.korisnikOcitanja);
        ispisiSvaOcitanjaZaKorisnikaUIntervalu(this.state.korisnikOcitanja).then(res => {
          if(res.status === 200){
            console.log(res.data);
            const {data} = res.data;
            if(data){
              this.setState({noviusers: data});
            }else{
              this.setState({noviusers: []});
            }
          }else{
           console.log("ne radi get user vrijeme po id i intervalu");
          }}
        ).catch(error => {
          console.log(error);
        })
      }
      else if(this.state.korisnikOcitanja.idKorisnika !== ""){
        console.log("dohvat ocitanja po id ", this.state.korisnikOcitanja.idKorisnika);
        ispisiSvaOcitanjaZaKorisnika(this.state.korisnikOcitanja).then(res => {
          if(res.status === 200){
            console.log(res.data);
            const {data} = res.data;
            if(data){
              this.setState({noviusers: data});
            }else{
              this.setState({noviusers: []});
            }
          }else{
           console.log("ne radi get user vrijeme po id");
          }}
        ).catch(error => {
          console.log(error);
        })
      }
      else if((this.state.korisnikOcitanja.datumOd !== "" && this.state.korisnikOcitanja.datumDo !== "") &&
      (this.state.korisnikOcitanja.datumOd <= this.state.korisnikOcitanja.datumDo)) {
        console.log("dohvat ocitanja", this.state.korisnikOcitanja.datumOd, this.state.korisnikOcitanja.datumDo);
        getRegisteredReadingInRange(this.state.korisnikOcitanja).then(res => {
          if(res.status === 200){
            console.log(res.data);
            const {data} = res.data;
            if(data){
              this.setState({noviusers: data});
            }else{
              this.setState({noviusers: []});
            }
          }else{
           console.log("ne radi get user vrijeme u intervalu");
          }}
        ).catch(error => {
          console.log(error);
        })
      }
    }

    showPeopleByNameAndLastname = () => {
      const noviusers1 = this.state.noviusers.filter(u => ((u.ime === this.state.imeUsporedba || this.state.imeUsporedba === "") &&
      (u.prezime === this.state.prezimeUsporedba || this.state.prezimeUsporedba === "") &&
      (u.ID === this.state.korisnikOcitanja.idKorisnika || this.state.korisnikOcitanja.idKorisnika === "")));
      this.setState({users: noviusers1, prvoOcitanjeKojeSePrikazuje: 0});
    };

    showPeopleByNameAndLastnameDefault = () => {
      const fiksniusers1 = this.state.fiksniusers;
      this.setState({noviusers: fiksniusers1});
      const noviusers2 = this.state.noviusers.filter(u => ((u.ime === this.state.imeUsporedba || this.state.imeUsporedba === "") &&
      (u.prezime === this.state.prezimeUsporedba || this.state.prezimeUsporedba === "") &&
      (u.ID === this.state.korisnikOcitanja.idKorisnika || this.state.korisnikOcitanja.idKorisnika === "")));
      this.setState({users: noviusers2, prvoOcitanjeKojeSePrikazuje: 0});
    };

    checkAndShowPeople = () => {
      if(this.state.korisnikOcitanja.datumOd !== "" && this.state.korisnikOcitanja.datumDo !== "" &&
      (this.state.korisnikOcitanja.datumOd <= this.state.korisnikOcitanja.datumDo)){
        console.log("oba datuma su razlicita od 0");
        this.showPeopleByNameAndLastname();
      }
      else if(this.state.korisnikOcitanja.datumOd === "" && this.state.korisnikOcitanja.datumDo === ""){
        console.log("oba datuma su jednaka 0");
        this.showPeopleByNameAndLastnameDefault();
      }
      else if(this.state.korisnikOcitanja.datumOd !== "" && this.state.korisnikOcitanja.datumDo !== "" &&
      (this.state.korisnikOcitanja.datumOd > this.state.korisnikOcitanja.datumDo)){
        this.setState({showDateOrderError: true});
        this.setState({showDateErrorModal: true});
      }
      else {
        this.setState({showTwoDatesError: true});
        this.setState({showDateErrorModal: true});
      }
    }

    render() {
      console.log("render ", this.state.noviusers);

      const closeModalHandler = () => {
        this.setState({showDateErrorModal: false});
        this.setState({showTwoDatesError: false});
        this.setState({showDateOrderError: false});
      }

      const showPeople = () => {
          /*const filtriraneOsobe = (this.state.users as User[])
              .filter(user => (user as User).ime.startsWith(this.state.user) ||
                  (user as User).prezime.startsWith(this.state.user));
           */
          const poljeOsoba = this.state.users.map((user, index) => vratiRedak(user, index+1));
          return poljeOsoba.slice(this.state.prvoOcitanjeKojeSePrikazuje, this.state.prvoOcitanjeKojeSePrikazuje+10);
      }
  
      const vratiRedak = (user: UserTime, index: number) => {
        return <tr key={index}>
          <th scope="row">{index}</th>
            <td>{user.ID}</td>
            <td>{user.ime}</td>
            <td>{user.prezime}</td>
            <td>{user.pozicija}</td>
            <td>{user.vrijeme}</td>
            <td>{user.razlog}</td>
            <td>{user.lokacija}</td>
            <td>{user.uredaj}</td>
            <td>{user.automobil}</td>
          </tr>
      }

      const naprijed = () => {
        if(this.state.prvoOcitanjeKojeSePrikazuje + 10< this.state.users.length){
          this.setState({prvoOcitanjeKojeSePrikazuje: this.state.prvoOcitanjeKojeSePrikazuje + 10})
        }
      }

      const nazad = () => {
          if (this.state.prvoOcitanjeKojeSePrikazuje > 0) {
            this.setState({prvoOcitanjeKojeSePrikazuje: this.state.prvoOcitanjeKojeSePrikazuje - 10})
          }
      }

      return (
          <div>
            <Form as={Col}>
                <Form.Group as={Row}>
                    <Form.Group as={Col} sm="4">
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Ime:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="text" placeholder="Ime" value={this.state.imeUsporedba}
                                      onChange={(event: any) => this.setState({ imeUsporedba: event.target.value})}/>
                                   </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Prezime:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="text" placeholder="Prezime" value={this.state.prezimeUsporedba}
                                      onChange={(event: any) => this.setState({ prezimeUsporedba: event.target.value})}/>
                                  </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Id korisnika:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="number" placeholder="Id korisnika" value={this.state.korisnikOcitanja.idKorisnika}
                                      onChange={(event: any) => this.setState({ korisnikOcitanja: {...this.state.korisnikOcitanja, idKorisnika: event.target.value}})}/>
                                  </Col>
                            </Form.Group>
                    </Form.Group>

                    <Form.Group as={Col} sm ="5">
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Početni datum:</Form.Label>
                                  <Col sm="5">
                                      <Form.Control type="date" placeholder="Odaberite datum" value={this.state.korisnikOcitanja.datumOd}
                                      onChange={(event: any) => this.setState({korisnikOcitanja: { ...this.state.korisnikOcitanja, datumOd: event.target.value }})}/>
                                  </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Završni datum:</Form.Label>
                                  <Col sm="5">
                                      <Form.Control type="date" placeholder="Odaberite datum" value={this.state.korisnikOcitanja.datumDo}
                                      onChange={(event: any) => this.setState({korisnikOcitanja: { ...this.state.korisnikOcitanja, datumDo: event.target.value }})}/>
                                  </Col>
                             </Form.Group>
                             <Form.Group as={Row}>
                                   <Col sm="5">
                                      <Button onClick={this.checkAndShowPeople}>Prikaži</Button>
                                   </Col>
                             </Form.Group>
                    </Form.Group>
                </Form.Group>
            </Form>

            <Modal show={this.state.showDateErrorModal}>
            <Modal.Body>
              <Form>
                  {this.state.showTwoDatesError?<Alert variant="info">Unesite tražene datume u oba polja!</Alert>:null}
                  {this.state.showDateOrderError?<Alert variant="info">Redoslijed datuma je neispravno unesen!</Alert>:null}
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Col sm="4"></Col>
              <Col sm="3"><Button variant="primary" onClick={closeModalHandler}>Close</Button></Col>
              <Col sm="4"></Col>
            </Modal.Footer>
          </Modal>

            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Redni broj</th>
                            <th scope="col">Id korisnika</th>
                            <th scope="col">Ime</th>
                            <th scope="col">Prezime</th>
                            <th scope="col">Radno mjesto</th>
                            <th scope="col">Vrijeme očitanja</th>
                            <th scope="col">Razlog</th>
                            <th scope="col">Lokacija</th>
                            <th scope="col">Uređaj</th>
                            <th scope="col">Vozilo</th>
                        </tr>
                    </thead>
                    <tbody>
                        {showPeople()}
                    </tbody>
                </table>
                <div className="bg-info">
                  <Button variant="primary" className="btn btn-secondary float-left" onClick = {nazad} children={"<<"}/>
                  <Button variant="primary" className="btn btn-secondary float-right" onClick = {naprijed} children={">>"}/>
                </div>
            </div>
          </div>
      );
    }
  }

