import React, {Component} from "react";
import {Button, Form, Row, Col, Alert, Modal} from "react-bootstrap";
import { getUsers } from "../store/serviceZaposlenik";
import '../pages/design/table.css';
import {
 // izracunajRadnoVrijemeZaKorisnikaUIntervalu,
  izracunajRadnoVrijemeZaSveKorisnikeUIntervalu
} from "../store/serviceEvidencijaSati";
// import deepEqual from "deep-equal";

export interface PodaciZaRadnoVrijeme{
  datumOd: string,
  datumDo: string,
  ID: string
}

export interface UserWork{
  ime: string,
  prezime: string,
  datumRodenja: string,
  pozicija: string,
  ID: string
}

interface VremenskiRad {
  ID: string;
  vrijeme: string;
}

export class EvidencijaSati extends Component {
  
    state = {
      users: [{ ID: "", ime: "", prezime: "", datumRodenja: "", pozicija: ""}],
      newusers: [{ ID: "", ime: "", prezime: "", datumRodenja: "", pozicija: ""}],
      imeUsporedba: "",
      prezimeUsporedba: "",
      podaciZaRadnoVrijeme: {ID: "", datumOd: "", datumDo: ""} as PodaciZaRadnoVrijeme,
      radnoVrijeme: "----",
      showWorkTime: "----",
      defaultTime: "----",
      showTwoDatesError: false,
      showDateOrderError: false,
      showDateErrorModal: false,
      prvoOcitanjeKojeSePrikazuje: 0,
      vrijemeZaKorisnika: [{ID: "", vrijeme: "----"} as VremenskiRad],
      pretVrijemeOd: "",
      pretVrijemeDo: "",
      pretId: "-1"
    }

    componentDidMount(): void {
      getUsers().then(res => {
        if(res.status === 200){
          console.log(res.data);
          const {data} = res.data;
          if(data) {
            const vremenaZaKorisnike = [] as VremenskiRad[];
            data.forEach((user: any) => vremenaZaKorisnike.push({ID: user.ID, vrijeme: "----"}));
            this.setState({users: data});
            this.setState({newusers: data, vrijemeZaKorisnika: vremenaZaKorisnike});
          }else{
            this.setState({users: []});
            this.setState({newusers: [], vrijemeZaKorisnika: [{ID: "", vrijeme: "----"}]});
          }
        }else{
          console.log("ne radi get user vrijeme");
        }}
      ).catch(error => {
        console.log(error);
      })
    }

    setDefaultValues = (users: any) => {
      const vremenaZaKorisnike = [] as VremenskiRad[];
      users.forEach((user: any) => vremenaZaKorisnike.push({ID: user.ID, vrijeme: "----"}));
      this.setState({vrijemeZaKorisnika: vremenaZaKorisnike});
    }
/*
    componentDidUpdate(): void {
      if(this.state.podaciZaRadnoVrijeme.ID !== "" && this.state.podaciZaRadnoVrijeme.datumOd !== "" && 
      this.state.podaciZaRadnoVrijeme.datumDo !== ""){
        if (this.state.pretId !== this.state.podaciZaRadnoVrijeme.ID) {
          izracunajRadnoVrijemeZaKorisnikaUIntervalu(this.state.podaciZaRadnoVrijeme).then(res => {
                if (res.status === 200) {
                  console.log(res.data);
                  const {brojSati} = res.data;
                  if (brojSati) {
                    const vrijeme = this.setTimeForUser(this.state.podaciZaRadnoVrijeme.ID, brojSati.toString());
                    this.setState({radnoVrijeme: brojSati, vrijemeZaKorisnika: vrijeme, pretId: this.state.podaciZaRadnoVrijeme.ID});
                  } else {
                    const vrijeme = this.setTimeForUser(this.state.podaciZaRadnoVrijeme.ID, "0");
                    this.setState({radnoVrijeme: "0", vrijemeZaKorisnika: vrijeme, pretId: this.state.podaciZaRadnoVrijeme.ID});
                  }
                } else {
                  console.log("ne radi get user vrijeme u intervalu");
                }
              }
          ).catch(error => {
            console.log(error);
          })
        }
      }
      else if (this.state.podaciZaRadnoVrijeme.ID === "" && this.state.podaciZaRadnoVrijeme.datumOd !== "" &&
          this.state.podaciZaRadnoVrijeme.datumDo !== ""){
        console.log("ndfksd");
        if (this.state.pretVrijemeOd !== this.state.podaciZaRadnoVrijeme.datumOd ||
            this.state.pretVrijemeDo !== this.state.podaciZaRadnoVrijeme.datumDo)
        izracunajRadnoVrijemeZaSveKorisnikeUIntervalu(
            {datumOd: this.state.podaciZaRadnoVrijeme.datumOd,
          datumDo: this.state.podaciZaRadnoVrijeme.datumDo}).then(res => {
            if (res.status===200) {
              console.log(res.data);
              if(res.data){
                const vrijeme = this.setTimesForUsers(res.data);
                this.setState({vrijemeZaKorisnika: vrijeme, pretVrijemeOd: this.state.podaciZaRadnoVrijeme.datumOd,
                  pretVrijemeDo: this.state.podaciZaRadnoVrijeme.datumDo});
              }
            }
        })
          .catch(e => {});
      }
    }

 */

    getUserWorkTime = () => {
      if (this.state.podaciZaRadnoVrijeme.datumOd !== "" && this.state.podaciZaRadnoVrijeme.datumDo !== ""){
        izracunajRadnoVrijemeZaSveKorisnikeUIntervalu(
            {datumOd: this.state.podaciZaRadnoVrijeme.datumOd,
              datumDo: this.state.podaciZaRadnoVrijeme.datumDo}).then(res => {
          if (res.status===200) {
            console.log(res.data);
            if(res.data){
              const vrijeme = this.setTimesForUsers(res.data);
              this.setState({vrijemeZaKorisnika: vrijeme, pretVrijemeOd: this.state.podaciZaRadnoVrijeme.datumOd,
                pretVrijemeDo: this.state.podaciZaRadnoVrijeme.datumDo});
            }
          }
        }).catch(e => {});
      }
    }

    setTimesForUsers = (poljeVremena: any) => {
      const {data} = poljeVremena;
      console.log(data);
      const vrijemeZaKorisnike = [];
      for(let i = 0;i<data.length;i++){
        vrijemeZaKorisnike.push({ID: data[i].ID, vrijeme: data[i].ukupni_sati});
      }
      return vrijemeZaKorisnike;
    }
/*
    setTimeForUser = (ID: string, vrijeme: string) => {
      const vrijemeZaKorisnike = [...this.state.vrijemeZaKorisnika];
      vrijemeZaKorisnike.filter((vrijeme) => vrijeme.ID !== ID);
      vrijemeZaKorisnike.push({ID: ID, vrijeme: vrijeme});
      return vrijemeZaKorisnike;
    }

 */

    /*
    showPersonWorkTime = () => {
      const zamjena = this.state.radnoVrijeme;
      this.setState({showWorkTime: zamjena});
      const noviusers1 = this.state.users.filter(u => ((u.ime === this.state.imeUsporedba || this.state.imeUsporedba === "") &&
      (u.prezime === this.state.prezimeUsporedba || this.state.prezimeUsporedba === "") &&
      (u.ID === this.state.podaciZaRadnoVrijeme.ID || this.state.podaciZaRadnoVrijeme.ID === "")));
      if(this.checkDifference(this.state.newusers, noviusers1)) {
        // satvi na svoj rest api tako da bude jednostavnije
        this.setState({newusers: noviusers1, prvoOcitanjeKojeSePrikazuje: 0});
      }
    };

    showPersonWorkTimeDefault = () => {
      const zamjena = this.state.defaultTime;
      this.setState({showWorkTime: zamjena});
      const noviusers1 = this.state.users.filter(u => ((u.ime === this.state.imeUsporedba || this.state.imeUsporedba === "") &&
      (u.prezime === this.state.prezimeUsporedba || this.state.prezimeUsporedba === "") &&
      (u.ID === this.state.podaciZaRadnoVrijeme.ID || this.state.podaciZaRadnoVrijeme.ID === "")));
      if(this.checkDifference(this.state.newusers, noviusers1)) {
        // premjestit ovaj setState u drugu funkciju koja će sadržavat ono što ima componentDidUpdate tako da je na klik gumba
        this.setState({newusers: noviusers1, prvoOcitanjeKojeSePrikazuje: 0});
      }
    };

     */

    checkAndShow = () => {

      // slucaj jedan datum ispunjen drugi nije
      if(this.state.podaciZaRadnoVrijeme.datumOd !== "" && this.state.podaciZaRadnoVrijeme.datumDo !== "" &&
      (this.state.podaciZaRadnoVrijeme.datumOd <= this.state.podaciZaRadnoVrijeme.datumDo)){
        console.log("oba datuma su razlicita od 0");
        this.getUserWorkTime();
        /*
        if(this.state.podaciZaRadnoVrijeme.ID === ""){
          this.setState({radnoVrijeme: "----"});
          this.showPersonWorkTimeDefault();
        }
        else{
          this.showPersonWorkTime();
        }
         */
      }
      else if(this.state.podaciZaRadnoVrijeme.datumOd === "" && this.state.podaciZaRadnoVrijeme.datumDo === ""){
        console.log("oba datuma su jednaka 0");
        this.setState({radnoVrijeme: "----"});
        // za ovaj slučaj prikazi sve prazno sa -----
       // this.showPersonWorkTimeDefault();
        this.setDefaultValues(this.state.users);
      }
      else if(this.state.podaciZaRadnoVrijeme.datumOd !== "" && this.state.podaciZaRadnoVrijeme.datumDo !== "" &&
      (this.state.podaciZaRadnoVrijeme.datumOd > this.state.podaciZaRadnoVrijeme.datumDo)){
        this.setState({showDateOrderError: true});
        this.setState({showDateErrorModal: true});
      }
      else {
        this.setState({showTwoDatesError: true});
        this.setState({showDateErrorModal: true});
      }
    }
  /*
    private checkDifference(array1: any[], array2: any[]){
      if(array1.length === array2.length){
        for(let i in array1){
          if(!deepEqual(array1[i], array2[i])){
            return true;
          }
        }
        return false;
      }
      return true;
    }

   */
  
    render() {
      console.log(this.state.newusers);

      const closeModalHandler = () => {
        this.setState({showDateErrorModal: false});
        this.setState({showTwoDatesError: false});
        this.setState({showDateOrderError: false});
      }

      const filterPeople = () => {
        return this.state.users.filter(u => ((u.ime === this.state.imeUsporedba || this.state.imeUsporedba === "") &&
            (u.prezime === this.state.prezimeUsporedba || this.state.prezimeUsporedba === "") &&
            (u.ID === this.state.podaciZaRadnoVrijeme.ID || this.state.podaciZaRadnoVrijeme.ID === "")));
      }
  
      const showPeople = () => {
          const osobe = filterPeople();
          const poljeOsoba = osobe.map(user => vratiRedak(user));
          return poljeOsoba.slice(this.state.prvoOcitanjeKojeSePrikazuje, this.state.prvoOcitanjeKojeSePrikazuje+10);
      }

      const getTimeForUser = (ID: string) => {
        let time = "----";
        this.state.vrijemeZaKorisnika.forEach( (vrijeme) =>
        {
          if (vrijeme.ID===ID) {
            time = vrijeme.vrijeme;
          }
        })
        return time;
      }
  
      const vratiRedak = (user: UserWork) => {
        return <tr>
          <th scope="row">{user.ID}</th>
            <td>{user.ime}</td>
            <td>{user.prezime}</td>
            <td>{user.pozicija}</td>
            <td>{getTimeForUser(user.ID)}</td>
          </tr>
      }

      const naprijed = () => {
        if(this.state.prvoOcitanjeKojeSePrikazuje + 10< this.state.newusers.length){
          this.setState({prvoOcitanjeKojeSePrikazuje: this.state.prvoOcitanjeKojeSePrikazuje + 10})
        }
      }

      const nazad = () => {
        if (this.state.prvoOcitanjeKojeSePrikazuje > 0) {
          this.setState({prvoOcitanjeKojeSePrikazuje: this.state.prvoOcitanjeKojeSePrikazuje - 10})
        }
      }

      return (
          <div>
            <Form as={Col}>
                <Form.Group as={Row}>
                    <Form.Group as={Col} sm="4">
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Ime:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="text" placeholder="Ime" value={this.state.imeUsporedba}
                                      onChange={(event: any) => this.setState({ imeUsporedba: event.target.value})}/>
                                   </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Prezime:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="text" placeholder="Prezime" value={this.state.prezimeUsporedba}
                                      onChange={(event: any) => this.setState({ prezimeUsporedba: event.target.value})}/>
                                  </Col>
                            </Form.Group>
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Id korisnika:</Form.Label>
                                  <Col sm="7">
                                      <Form.Control type="number" placeholder="Id korisnika" value={this.state.podaciZaRadnoVrijeme.ID}
                                      onChange={(event: any) => this.setState({podaciZaRadnoVrijeme: { ...this.state.podaciZaRadnoVrijeme, ID: event.target.value}})}/>
                                  </Col>                
                            </Form.Group>                
                    </Form.Group>

                    <Form.Group as={Col} sm ="5">
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Početni datum:</Form.Label>
                                  <Col sm="5">
                                      <Form.Control type="date" placeholder="Odaberite datum" value={this.state.podaciZaRadnoVrijeme.datumOd} 
                                      onChange={(event: any) => this.setState({podaciZaRadnoVrijeme: { ...this.state.podaciZaRadnoVrijeme, datumOd: event.target.value }})}/>
                                  </Col>
                            </Form.Group>                
                            <Form.Group as={Row}>
                                  <Form.Label column sm="3">Završni datum:</Form.Label>
                                  <Col sm="5">
                                      <Form.Control type="date" placeholder="Odaberite datum" value={this.state.podaciZaRadnoVrijeme.datumDo}
                                      onChange={(event: any) => this.setState({podaciZaRadnoVrijeme: { ...this.state.podaciZaRadnoVrijeme, datumDo: event.target.value }})}/>
                                  </Col>                  
                             </Form.Group>
                             <Form.Group as={Row}>
                                   <Col sm="5">
                                      <Button onClick={this.checkAndShow}>Prikaži</Button>
                                   </Col>
                             </Form.Group> 
                    </Form.Group>                     
                </Form.Group>                             
            </Form>

            <Modal show={this.state.showDateErrorModal}>
            <Modal.Body>
              <Form>
                  {this.state.showTwoDatesError?<Alert variant="info">Unesite tražene datume u oba polja!</Alert>:null}
                  {this.state.showDateOrderError?<Alert variant="info">Redoslijed datuma je neispravno unesen!</Alert>:null}
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <Col sm="4"></Col>
              <Col sm="3"><Button variant="primary" onClick={closeModalHandler}>Close</Button></Col>
              <Col sm="4"></Col>
            </Modal.Footer>
          </Modal>
            
            <div className="container" >
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Ime</th>
                            <th scope="col">Prezime</th>
                            <th scope="col">Radno mjesto</th>
                            <th scope="col">Broj radnih sati</th>
                        </tr>
                    </thead>
                    <tbody>
                        {showPeople()}
                    </tbody>
                </table>
                <div className="bg-info">
                  <Button variant="primary" className="btn btn-secondary float-left" onClick = {nazad} children={"<<"}/>
                  <Button variant="primary" className="btn btn-secondary float-right" onClick = {naprijed} children={">>"}/>
                </div>
            </div>
          </div>
      );
    }
  }


