import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {Prijava} from "./pages/Prijava";
import {PageForEmploye} from "./pages/PageForEmploye";

const App: React.FC = () => {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/build/" component={Prijava}/>
          <Route path="/build/employe" component={PageForEmploye}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
