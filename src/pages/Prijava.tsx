import React, {Component} from "react";
import {login} from "../store/serviceLogin";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './design/signIn.css';
import {Redirect} from "react-router";
import {Alert} from "react-bootstrap";

export class Prijava extends Component {

  state = {
    user: "",
    password: "",
    toEmploye: false,
    netocniLogin: false
  };

  render() {
    if(this.state.toEmploye){
      return <Redirect to="/build/employe" />
    }

    const tryingToLogin = () => {
      const a: any = {
        username: this.state.user,
        password: this.state.password
      }
      login(a)
          .then(res => {
            if (res.status === 200) {
              this.setState({toEmploye: true});
            }
            else{
              this.setState({netocniLogin: true});
            }
          }).catch(error => {});
    }

    return (
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
              <Form className="login100-form validate-form flex-sb flex-w">

            <span className={"login100-form-title"}>
						  Login
					  </span>
                <Form.Group controlId="formUsername">
                  <Form.Label>Username</Form.Label>
                  <Form.Control type="username" placeholder="Enter username"
                                name="user"
                                value={this.state.user}
                                onChange={this.settingUser}/>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="Password"
                                name="password"
                                value={this.state.password}
                                onChange={this.settingPassword}/>
                </Form.Group>
                <div className={"elements"}>
                  <Button onClick={tryingToLogin}>
                    Log in
                  </Button>
                  <span>

                  </span>
                </div>
                {this.state.netocniLogin?
                  <Alert variant={"warning"}>
                    Netočna lozinka ili korisnički račun
                  </Alert>: null
                }
              </Form>
            </div>
          </div>
        </div>
    );
  }

  private settingUser = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, user: text})
  }

  private settingPassword = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, password: text})
  }

}



