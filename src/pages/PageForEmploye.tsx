import React, {Component} from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import {Route, Switch} from "react-router";
import {Osobe} from "../components/osobe";
import {Device} from "../components/Device";
import {EvidencijaSati} from "../components/evidencijaSati";
import {Redirect} from "react-router";
import { EvidencijaOcitanja } from "../components/evidencijaOcitanja";
import {Cars} from "../components/Cars";

export class PageForEmploye extends Component {

  state = {
    goToLogin: false
  }

  render(){
    if(this.state.goToLogin){
      sessionStorage.clear();
      return <Redirect to="/build" />
    }
    if(!sessionStorage.getItem("userInfo")){
      return <Redirect to="/build"/>
    }

    const going = () => {
      this.setState({goToLogin: true});
    }

    return(
        <div>
          <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
            <Navbar.Brand href="/build/employe/">Evidencija vremena</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/build/employe/">Zaposlenici</Nav.Link>
                <Nav.Link href="/build/employe/devices">Uredaji</Nav.Link>
                <Nav.Link href="/build/employe/cars">Automobili</Nav.Link>
                <Nav.Link href="/build/employe/sati">Radni sati</Nav.Link>
                <Nav.Link href="/build/employe/ocitanja">Očitanja</Nav.Link>
              </Nav>
              <Nav>
                <Nav.Link eventKey={2} onClick={going}>
                  Odjava
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>

          <Switch>
            <Route exact path="/build/employe/" component={Osobe}/>
            <Route exact path="/build/employe/devices" component={Device}/>
            <Route exact path="/build/employe/cars" component={Cars}/>
            <Route exact path="/build/employe/sati" component={EvidencijaSati}/>
            <Route exact path="/build/employe/ocitanja" component={EvidencijaOcitanja}/>
          </Switch>
        </div>
    );
  }

}

