import React, {Component} from "react";
import {signin} from "../store/service";
import {Link, Redirect} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './design/signIn.css';

export class Prijava extends Component{

  constructor(props: Readonly<{}>){
    super(props);
    this.callAction = this.callAction.bind(this);
  }
  state = {
    user: "",
    password: "",
    toHomePage: false
  };

  render() {
    if(this.state.toHomePage){
      return(
      <Redirect to="/employe" />
      );
    }
    return (
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
              <Form className="login100-form validate-form flex-sb flex-w">

            <span className={"login100-form-title"}>
						  Login
					  </span>
            <Form.Group controlId="formUsername">
             <Form.Label>Username</Form.Label>
             <Form.Control type="username" placeholder="Enter username"
                           name="user"
                           value={this.state.user}
                           onChange={this.settingUser}/>
           </Form.Group>

           <Form.Group controlId="formBasicPassword">
             <Form.Label>Password</Form.Label>
             <Form.Control type="password" placeholder="Password"
                           name="password"
                           value={this.state.password}
                           onChange={this.settingPassword}/>
           </Form.Group>
          <div className={"elements"}>
           <Button onClick={this.callAction}>
             Log in
           </Button>
            <span/>
            <span/>
           <Link to="/registracija">
             {"don't have an account? sign up"}
           </Link>
            </div>
         </Form>
        </div>
          </div>
        </div>
    );
  }

  callAction = (dispatch: any) => {
     const success: boolean = signin(this.state.user, this.state.password);
    if(success===true) {
      this.setState({...this.state, toHomePage: true});
    }
  }

  private settingUser = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, user: text})
  }

  private settingPassword = (event: any) => {
    const text = event.target.value;
    this.setState({...this.state, password: text})
  }

}



