import React, {Component} from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import {Redirect, Route, Switch} from "react-router";
import {Osobe} from "../components/osobe";

export class PageForEmploye extends Component {

  state = {
    odjava: false
  }

  render(){
    if(this.state.odjava){
      return(<Redirect to={"/"}/>)
    }
    return(
        <div>
          <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
            <Navbar.Brand href="/employe/">evidencija vremena</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/employe/pretragaPoOsobama">pretraga po osobama</Nav.Link>
                <Nav.Link eventKey={2} onClick={this.odjava}>
                  Odjava
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>

          <Switch>
            <Route exact path="/employe/pretragaPoOsobama" component={Osobe}/>
          </Switch>
        </div>
    );
  }

  odjava = () => {
    this.setState({odjava: true});
  }
}
