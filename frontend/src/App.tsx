import React from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Prijava} from "./pages/Prijava";
import {PageForEmploye} from "./pages/PageForEmploye";

const App: React.FC = () => {
  return (
      <Router>
        <Switch>
          <Route exact path="/">
            <Prijava />
          </Route>
          <Route path="/employe">
            <PageForEmploye/>
          </Route>
        </Switch>
      </Router>
  );
}

export default App;
