import React, {Component} from "react";
import {DropdownButton, FormControl, InputGroup, Table, Dropdown} from "react-bootstrap";
import {bla, fetchAllUsers} from "../store/service";

export class Osobe extends Component {

  state = {
    view: "sati po mjesecu",
    user: "",
    users: [{}]
  }

  constructor(props: any) {
    super(props);
    this.state.users = fetchAllUsers();
  }


  render() {

    const changingView = (broj: number) => {
      if(broj===2)
        this.setState({view: "min sati u danu"})
      else if(broj===3)
        this.setState({view: "max sati u danu"})
      else
        this.setState({view: "sati po mjesecu"})
    }

    const showPeople = () => {
      const filtriraneOsobe = (this.state.users as bla[])
          .filter(user => user.Ime.startsWith(this.state.user) ||
              user.Prezime.startsWith(this.state.user))
      const poljeOsoba = filtriraneOsobe.map(user => vratiRedak(user))
      return poljeOsoba;
    }

    const vratiRedak = (user: bla) => {
      let data = "";
      if(this.state.view==="sati po mjesecu"){
        data = user.mjes;
      }else if(this.state.view==="max sati u danu"){
        data = user.max
      }
      else{
        data = user.min
      }
      return <div>
        <tr>
          <th>{user.Ime}</th>
          <th>{user.Prezime}</th>
          <th>{data}</th>
        </tr>
      </div>
    }

    return (
        <div>
        <InputGroup>
            <FormControl
                placeholder="Ime i prezime"
                aria-label="Recipient's username"
                aria-describedby="basic-addon2"
                onChange={(event: any) => this.setState({user: event.target.value})}
                value={this.state.user}
            />

            <DropdownButton
                as={InputGroup.Append}
                variant="outline-secondary"
                title="Dropdown"
                id="input-group-dropdown-2"
            >
                <Dropdown.Item onClick={() => changingView(1)}>sati po mjesecu</Dropdown.Item>
                <Dropdown.Item onClick={() => changingView(2)}>min sati u danu</Dropdown.Item>
                <Dropdown.Item onClick={() => changingView(3)}>max sati u danu</Dropdown.Item>
            </DropdownButton>
        </InputGroup>


        <Table bordered hover>
          <thead>
          <tr>
            <th>Ime</th>
            <th>Prezime</th>
            <th>{this.state.view}</th>
          </tr>
          </thead>
          <tbody>
            {showPeople()}
          </tbody>
        </Table>
        </div>
    );
  }

}
