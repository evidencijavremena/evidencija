<?php
include_once 'Uredaj.php';
include_once 'Ocitanje.php';

class Korisnik{
    private $conn;
    private $table = "korisnik";

    public $ID;
    public $ime;
    public $prezime;
    public $datumRodenja;
    public $pozicija;
    public $aktivan;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}