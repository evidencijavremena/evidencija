<?php
class Ocitanje{
    private $conn;
    private $table = "ocitanje";

    public $ID;
    public $korisnik_ID;
    public $vrijeme;
    public $lokacija;
    public $razlog_ID;
    public $uredaj_ID;
    public $automobil_ID;

    public function __construct($db)
    {
        $this->conn = $db;
    }

}