<?php
include_once 'Korisnik.php';
include_once 'Uredaj.php';
include_once 'Automobil.php';

class Tajnica{
    private $conn;
    private $table = "admin";
    private $table_kor = "korisnik";
    private $table_ur = "uredaj";
    private $table_au = "automobil";
    public $username;
    public $password;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function ispisiSvaOcitanjaZaKorisnika($id){
        $query = "SELECT kor.ID, kor.ime, kor.prezime, kor.pozicija, DATE_FORMAT(ocitanje.vrijeme, '%d.%m.%Y %H:%i:%s') AS vrijeme, razlog.razlog,
                        ocitanje.lokacija, uredaj.naziv AS uredajNaziv, automobil.naziv AS automobilNaziv  
                FROM korisnik AS kor 
                LEFT JOIN ocitanje ON kor.ID = ocitanje.korisnik_ID
                LEFT JOIN razlog ON ocitanje.razlog_ID = razlog.ID
                LEFT JOIN uredaj ON ocitanje.uredaj_ID = uredaj.ID
                LEFT JOIN automobil ON ocitanje.automobil_ID = automobil.ID
                WHERE ocitanje.korisnik_ID = :id AND ocitanje.vrijeme IS NOT null
                ORDER BY ocitanje.vrijeme DESC";

        $stmt = $this->conn->prepare($query);
        $id = htmlspecialchars(strip_tags($id));
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiSvaOcitanjaZaKorisnikaUIntervalu($id, $datumOd, $datumDo){
        $query = "SELECT kor.ID, kor.ime, kor.prezime, kor.pozicija, DATE_FORMAT(ocitanje.vrijeme, '%d.%m.%Y %H:%i:%s') AS vrijeme, razlog.razlog,
                        ocitanje.lokacija, uredaj.naziv AS uredajNaziv, automobil.naziv AS automobilNaziv  
                FROM korisnik AS kor 
                LEFT JOIN ocitanje ON kor.ID = ocitanje.korisnik_ID
                LEFT JOIN razlog ON ocitanje.razlog_ID = razlog.ID
                LEFT JOIN uredaj ON ocitanje.uredaj_ID = uredaj.ID
                LEFT JOIN automobil ON ocitanje.automobil_ID = automobil.ID
                WHERE ocitanje.korisnik_ID = :id AND (DATE(ocitanje.vrijeme) BETWEEN  DATE(:dOd) AND DATE(:dDo))
                ORDER BY ocitanje.vrijeme DESC";

        $stmt = $this->conn->prepare($query);
        $id = htmlspecialchars(strip_tags($id));
        $datumOd = htmlspecialchars(strip_tags($datumOd));
        $datumDo = htmlspecialchars(strip_tags($datumDo));
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':dOd', $datumOd);
        $stmt->bindParam(':dDo', $datumDo);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiSvaOcitanja(){
        $query = "SELECT kor.ID, kor.ime, kor.prezime, kor.pozicija, DATE_FORMAT(ocitanje.vrijeme, '%d.%m.%Y %H:%i:%s') AS vrijeme, razlog.razlog,
                        ocitanje.lokacija, uredaj.naziv AS uredajNaziv, automobil.naziv AS automobilNaziv
                FROM korisnik AS kor
                LEFT JOIN ocitanje ON kor.ID = ocitanje.korisnik_ID
                LEFT JOIN razlog ON ocitanje.razlog_ID = razlog.ID
                LEFT JOIN uredaj ON ocitanje.uredaj_ID = uredaj.ID
                LEFT JOIN automobil ON ocitanje.automobil_ID = automobil.ID
                WHERE ocitanje.vrijeme IS NOT null
                ORDER BY ocitanje.vrijeme DESC, kor.prezime, kor.ime, kor.ID
                LIMIT 500";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiSvaOcitanjaUIntervalu($datumOd, $datumDo){
        $query = "SELECT kor.ID, kor.ime, kor.prezime, kor.pozicija, DATE_FORMAT(ocitanje.vrijeme, '%d.%m.%Y %H:%i:%s') AS vrijeme, razlog.razlog,
                          ocitanje.lokacija, uredaj.naziv AS uredajNaziv, automobil.naziv AS automobilNaziv  
                FROM korisnik AS kor 
                LEFT JOIN ocitanje ON kor.ID = ocitanje.korisnik_ID
                LEFT JOIN razlog ON ocitanje.razlog_ID = razlog.ID
                LEFT JOIN uredaj ON ocitanje.uredaj_ID = uredaj.ID
                LEFT JOIN automobil ON ocitanje.automobil_ID = automobil.ID
                WHERE (DATE(ocitanje.vrijeme) BETWEEN  DATE(:dOd) AND DATE(:dDo))
                ORDER BY ocitanje.vrijeme DESC, kor.prezime, kor.ime, kor.ID";

        $stmt = $this->conn->prepare($query);
        $datumOd = htmlspecialchars(strip_tags($datumOd));
        $datumDo = htmlspecialchars(strip_tags($datumDo));
        $stmt->bindParam(':dOd', $datumOd);
        $stmt->bindParam(':dDo', $datumDo);
        $stmt->execute();

        return $stmt;       
    }

    public function ispisiKorisnike(){
        $query = "SELECT ID, prezime, ime, datumRodenja, pozicija FROM korisnik
                  WHERE korisnik.aktivan = true
                  ORDER BY pozicija, ID, prezime, ime, datumRodenja";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiAutomobile(){
        $query = "SELECT ID, naziv FROM automobil WHERE aktivan = true";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiUredaje(){
        $query = "SELECT ID, naziv, kod FROM uredaj WHERE aktivan = true";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function ispisiRazloge(){
        $query = "SELECT ID, razlog FROM razlog";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    public function dodajKorisnika($userK){
        $korI = $userK->ime;
        $korP = $userK->prezime;
        $korD = $userK->datumRodenja;
        $korPoz = $userK->pozicija;
        $query = 'INSERT INTO ' . $this->table_kor. ' SET ime = :korI, prezime = :korP, datumRodenja = :korD, pozicija = :korPoz';
        $stmt = $this->conn->prepare($query);
        $userK->ime = htmlspecialchars(strip_tags($userK->ime));
        $userK->prezime = htmlspecialchars(strip_tags($userK->prezime));
        $userK->datumRodenja = htmlspecialchars(strip_tags($userK->datumRodenja));
        $userK->pozicija = htmlspecialchars(strip_tags($userK->pozicija));

        $stmt->bindParam(':korI', $userK->ime);
        $stmt->bindParam(':korP', $userK->prezime);
        $stmt->bindParam(':korD', $userK->datumRodenja);
        $stmt->bindParam(':korPoz', $userK->pozicija);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }
    public function izbrisiKorisnika($userK){
        //Create query
        $korId = $userK->ID;
        $query = 'UPDATE ' . $this->table_kor . ' SET aktivan = false WHERE ID = :korId';
        $stmt = $this->conn->prepare($query);
        //Clean data
        $userK->id = htmlspecialchars(strip_tags($userK->ID));
        //Bind data
        $stmt->bindParam(':korId', $userK->ID);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }
    public function azurirajKorisnika($userK){
        // Create query
        $korID = $userK->ID;
        $korI = $userK->ime;
        $korP = $userK->prezime;
        $korD = $userK->datumRodenja;
        $korPoz = $userK->pozicija;
        $query = 'UPDATE ' . $this->table_kor . ' SET ime = :korI, prezime = :korP, datumRodenja = :korD, pozicija = :korPoz WHERE ID = :korID';
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Clean data
        $userK->ime = htmlspecialchars(strip_tags($userK->ime));
        $userK->prezime = htmlspecialchars(strip_tags($userK->prezime));
        $userK->datumRodenja = htmlspecialchars(strip_tags($userK->datumRodenja));
        $userK->pozicija = htmlspecialchars(strip_tags($userK->pozicija));
        $userK->ID = htmlspecialchars(strip_tags($userK->ID));
        //Bind data
        $stmt->bindParam(':korI', $userK->ime);
        $stmt->bindParam(':korP', $userK->prezime);
        $stmt->bindParam(':korD', $userK->datumRodenja);
        $stmt->bindParam(':korPoz', $userK->pozicija);
        $stmt->bindParam(':korID', $userK->ID);
        //Execute query
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function dodajUredaj($uredaj){
        $urN = $uredaj->naziv;
        $urKod = $uredaj->kod;
        $query = 'INSERT INTO ' . $this->table_ur. '(naziv, kod) VALUES  (:urN, :urKod)';
        $stmt = $this->conn->prepare($query);
        $uredaj->naziv = htmlspecialchars(strip_tags($uredaj->naziv));
        $uredaj->kod = htmlspecialchars(strip_tags($uredaj->kod));

        $stmt->bindParam(':urN', $uredaj->naziv);
        $stmt->bindParam(':urKod', $uredaj->kod);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function izbrisiUredaj($uredaj){
        //Create query
        $urID = $uredaj->ID;
        $query = 'UPDATE ' . $this->table_ur . ' SET aktivan = false WHERE ID = :urID';
        $stmt = $this->conn->prepare($query);
        //Clean data
        $uredaj->ID = htmlspecialchars(strip_tags($uredaj->ID));
        //Bind data
        $stmt->bindParam(':urID', $uredaj->ID);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function azurirajUredaj($uredaj){
        // Create query
        $urID = $uredaj->ID;
        $urNaziv = $uredaj->naziv;
        $urKod = $uredaj->kod;
        $query = 'UPDATE ' . $this->table_ur  . ' SET naziv = :urNaziv, kod = :urKod  WHERE ID = :urID';
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Clean data
        $uredaj->naziv = htmlspecialchars(strip_tags($uredaj->naziv));
        $uredaj->kod = htmlspecialchars(strip_tags($uredaj->kod));
        $uredaj->ID = htmlspecialchars(strip_tags($uredaj->ID));
        //Bind data
        $stmt->bindParam(':urNaziv', $uredaj->naziv);
        $stmt->bindParam(':urID', $uredaj->ID);
        $stmt->bindParam(':urKod', $uredaj->kod);
        //Execute query
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function dodajAutomobil($automobil){
        $auN = $automobil->naziv;
        $query = 'INSERT INTO ' . $this->table_au. ' SET naziv = :auN';
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':auN', $automobil->naziv);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function izbrisiAutomobil($automobil){
        //Create query
        $auID = $automobil->ID;
        $query = 'UPDATE ' . $this->table_au . ' SET aktivan = false WHERE ID = :auID';
        $stmt = $this->conn->prepare($query);
        //Clean data
        $automobil->id = htmlspecialchars(strip_tags($automobil->ID));
        //Bind data
        $stmt->bindParam(':auID', $automobil->ID);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function azurirajAutomobil($automobil){
        $auID = $automobil->ID;
        $auNaziv = $automobil->naziv;
        $query = 'UPDATE ' . $this->table_au  . ' SET naziv = :auNaziv  WHERE ID = :auID';
        // Prepare statement
        $stmt = $this->conn->prepare($query);
        // Clean data
        $automobil->naziv = htmlspecialchars(strip_tags($automobil->naziv));
        $automobil->ID = htmlspecialchars(strip_tags($automobil->ID));
        //Bind data
        $stmt->bindParam(':auNaziv', $automobil->naziv);
        $stmt->bindParam(':auID', $automobil->ID);
        //Execute query
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }


    function usernameExists(){
        $query = "SELECT username, password
            FROM " . $this->table . "
            WHERE username = ?
            LIMIT 0,1";

        $stmt = $this->conn->prepare($query);

        $this->username=htmlspecialchars(strip_tags($this->username));

        $stmt->bindParam(1, $this->username);

        $stmt->execute();

        $num = $stmt->rowCount();

        if($num>0){

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->username = $row['username'];
            $this->password = $row['password'];

            return true;
        }
        return false;
    }

    function vratiVremenaRazloga($razlog, $datum, $korisnik){
        $query = "SELECT vrijeme 
                FROM ocitanje 
                JOIN razlog ON ocitanje.razlog_ID = razlog.ID 
                WHERE DATE(ocitanje.vrijeme) = DATE(:dat)
                AND ocitanje.korisnik_ID = :kID
                AND razlog.razlog = :raz";

        $stmt = $this->conn->prepare($query);
        $korisnik->ID = htmlspecialchars(strip_tags($korisnik->ID));
        $datum = htmlspecialchars(strip_tags($datum));
        $razlog = htmlspecialchars(strip_tags($razlog));
        $stmt->bindParam(':dat', $datum);
        $stmt->bindParam(':kID', $korisnik->ID);
        $stmt->bindParam(':raz', $razlog);
        $stmt->execute();

        $podatci = array();
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            array_push($podatci, $row);
        }
        $brojac = count($podatci);
        $vremena = array();
        for($i = 0; $i < $brojac; $i++){
            $podatci2 = $podatci[$i];
            array_push($vremena, $podatci2['vrijeme']);
        }
        return $vremena;
    }

    function izbrojiSate($brojDolazaka, $vremenaDolaska, $vremenaOdlaska){
        $brSati = 0;

        for($i = 0; $i < $brojDolazaka; $i++){
            $dolazak = strtotime($vremenaDolaska[$i]);
            $odlazak = strtotime($vremenaOdlaska[$i]);
            $brSati = $brSati + (($odlazak - $dolazak) / (60 * 60));
        }

        return $brSati;
    }

     function izracunajRadnoVrijemeZaKorisnikaUDanu($datum, $korisnik){
         $vremenaDolazaka = $this->vratiVremenaRazloga('DOLAZAK', $datum, $korisnik);
         $vremenaOdlazaka = $this->vratiVremenaRazloga('ODLAZAK', $datum, $korisnik);
         $brojDolazaka = count($vremenaDolazaka);
         $brojOdlazaka = count($vremenaOdlazaka);

         if($brojDolazaka == $brojOdlazaka){
            $brojRadnihSati = $this->izbrojiSate($brojDolazaka, $vremenaDolazaka, $vremenaOdlazaka);
         } else {
             array_push($vremenaOdlazaka, ''. $datum . ' 23:59:59');
             $brojRadnihSati = $this->izbrojiSate($brojDolazaka, $vremenaDolazaka, $vremenaOdlazaka);
             $brojRadnihSati = round($brojRadnihSati);
         }
        return $brojRadnihSati;
    }

    public function izracunajRadnoVrijemeZaKorisnikaUIntervalu($korisnik, $datumOd, $datumDo){
        $datumOdTime = strtotime($datumOd);
        $datumDoTime = strtotime($datumDo);
        if ($datumOd > $datumDo) return -1;
        $brojDana = round(($datumDoTime - $datumOdTime) / (60 * 60 * 24)) + 1;
        $brojRadnihSati = 0;

        for ($i = 0; $i < $brojDana; $i++) {
            $datumOd = date('Y-m-d', strtotime('+'. $i .' day', $datumOdTime));
            $brojRadnihSati = $brojRadnihSati + $this->izracunajRadnoVrijemeZaKorisnikaUDanu($datumOd, $korisnik);
        }
        return $brojRadnihSati;
    }
}
