<?php
class Uredaj{
    private $conn;
    private $table = "uredaj";
    private $table_oc = "ocitanje";
    private $table_kk = "karticakorisnik";
    public $ID;
    public $naziv;
    public $kod;
    public $aktivan;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function kodPostoji(){
        $query = "SELECT kod
            FROM " . $this->table . "
            WHERE kod = ?
            LIMIT 0,1";

        $stmt = $this->conn->prepare($query);
        $this->kod = htmlspecialchars(strip_tags($this->kod));
        $stmt->bindParam(1, $this->kod);
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num>0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->kod = $row['kod'];
            return true;
        }
        return false;
    }

    public function dodajOcitanje($ocitanje){
        $query1 = 'SELECT vrijeme, razlog_ID  FROM ocitanje 
                  WHERE korisnik_ID = :kID
                  AND vrijeme = (SELECT MAX(vrijeme) FROM ocitanje WHERE korisnik_ID = :kID)';
        $stmt1 = $this->conn->prepare($query1);

        $stmt1->bindParam(':kID', $ocitanje->korisnik_ID);
        $stmt1->execute();

        $podatci = array();
        while($row = $stmt1->fetch(PDO::FETCH_ASSOC)){
            array_push($podatci, $row);
        }
        $podatci2 = $podatci[0];
        $razlog = $podatci2['razlog_ID'];
        if($razlog == $ocitanje->razlog_ID) return false;
        $korID = $ocitanje->korisnik_ID;
        $vr = $ocitanje->vrijeme;
        $lok = $ocitanje->lokacija;
        $idRaz = $ocitanje->razlog_ID;
        $idUr = $ocitanje->uredaj_ID;
        $idAu = $ocitanje->automobil_ID;
        $query2 = 'INSERT INTO ' . $this->table_oc. ' SET korisnik_ID = :korID, vrijeme = :vr, lokacija = :lok, 
                    razlog_ID = :idRaz, uredaj_ID = :idUr, automobil_ID = :idAu';
        $stmt = $this->conn->prepare($query2);

        $stmt->bindParam(':korID', $ocitanje->korisnik_ID);
        $stmt->bindParam(':vr', $ocitanje->vrijeme);
        $stmt->bindParam(':lok', $ocitanje->lokacija);
        $stmt->bindParam(':idRaz', $ocitanje->razlog_ID);
        $stmt->bindParam(':idUr', $ocitanje->uredaj_ID);
        $stmt->bindParam(':idAu', $ocitanje->automobil_ID);
        if ($stmt->execute()){
            return true;
        }else{
            //Print error if something goes wrong
            printf("Error: %s.\n", $stmt->error);
            return false;
        }
    }

    public function vratiIDzaNFC($karticaKorisink){
        $NFCid = $karticaKorisink->NFC_ID;
        $query = 'SELECT korisnik_ID FROM ' . $this->table_kk . ' WHERE NFC_ID = :nfc';
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':nfc', $karticaKorisink->NFC_ID);
        $stmt->execute();
        return $stmt;
    }

    public function vratiUredajID(){

        $query = 'SELECT ID FROM ' . $this->table . ' WHERE kod = :k';
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':k', $this->kod);
        $stmt->execute();
        return $stmt;
    }
}