<?php
// show error reporting
error_reporting(E_ALL);

// set your default time-zone
date_default_timezone_set('Europe/Zagreb');

// variables used for jwt
$key = "evidencija";
$iss = "http://localhost/evidencija/backEnd/api/loginTajnica.php";
$aud = "http://localhost/evidencija/backEnd/api/loginTajnica.php";
$exp = time()+86400;
$iat = 1356999524;
$nbf = 1357000000;
?>
