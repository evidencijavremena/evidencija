<?php
// Headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

include_once '../../baza/Database.php';
include_once '../models/Uredaj.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();
// Instantiate blog post object
$uredaj = new Uredaj($db);

$data = json_decode(file_get_contents("php://input"));

$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt) {

    // if decode succeed, show user details
    try {

        // decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        $uredaj->kod = $data->kod;
        $result = $uredaj->vratiUredajID();
        $num = $result->rowCount();
        if($num > 0){
            $arr = array();

            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                extract($row);

                $item = array(
                    'ID' => $ID
                );

                array_push($arr, $item);
            }

            echo json_encode($arr);
        } else {
            echo json_encode(
                array('message' => 'Ništa nije pronađeno.')
            );
        }

    }catch (Exception $e){

        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array(
            "message" => "Pristup odbijen.",
            "error" => $e->getMessage()
        ));
    }
}