<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');

include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

include_once '../../baza/Database.php';
include_once '../models/Korisnik.php';
include_once '../models/Tajnica.php';
// Instantiate DB & connect
$database = new Database();
$db = $database->connect();
// Instantiate blog post object
$userT = new Tajnica($db);
$userK = new Korisnik($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));
$jwt=isset($data->jwt) ? $data->jwt : "";

if($jwt){
    try {
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $userK->ID = $data->ID;

        if ($userT->izbrisiKorisnika($userK)){
            http_response_code(200);
            echo json_encode(array('message' => 'Korisnik uspješno izbrisan'));

        } else {
            echo json_encode(array('message' => 'Korisnik se ne može izbrisati'));
        }
    } catch (Exception $e){
        http_response_code(401);
        echo json_encode(array(
            "message" => "Pristup odbijen.",
            "error" => $e->getMessage()
        ));
    }
} else {
    http_response_code(401);
    echo json_encode(array("message" => "Pristup odbijen."));
}