<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;


include_once '../../baza/Database.php';
include_once '../models/Automobil.php';
include_once '../models/Tajnica.php';
// Instantiate DB & connect
$database = new Database();
$db = $database->connect();
// Instantiate blog post object
$user = new Tajnica($db);
$automobil = new Automobil($db);
$data = json_decode(file_get_contents("php://input"));

// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt){

    // if decode succeed, show user details
    try {

        // decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        // set user property values
        $automobil->naziv = $data->naziv;

// update the user record
        if($user->dodajAutomobil($automobil)){

// set response code
            http_response_code(200);

// response in json format
            echo json_encode(
                array(
                    "message" => "Automobil uspješno dodan.",
			"id" => $db->lastInsertedId()
                )
            );
        }

// message if unable to update user
        else{
            // set response code
            http_response_code(401);

            // show error message
            echo json_encode(array("message" => "Nije moguće dodati automobil."));
        }
    }

        // if decode fails, it means jwt is invalid
    catch (Exception $e){

        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array(
            "message" => "Pristup odbijen.",
            "error" => $e->getMessage()
        ));
    }
}

// show error message if jwt is empty
else {

    // set response code
    http_response_code(401);

    // tell the user access denied
    echo json_encode(array("message" => "Pristup odbijen."));
}
