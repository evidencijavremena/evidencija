<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

include_once '../../baza/Database.php';
include_once '../models/Tajnica.php';

$database = new Database();
$db = $database->connect();

$userT = new Tajnica($db);
$data = json_decode(file_get_contents("php://input"));

$userT->username = $data->username;
$username_exists = $userT->usernameExists();

include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

if($username_exists && password_verify($data->password,$userT->password)){

    $token = array(
        "iss" => $iss,
        "aud" => $aud,
        "iat" => $iat,
        "nbf" => $nbf,
        "exp" => $exp,
        "data" => array(
            "username" => $userT->username
        )
    );

    http_response_code(200);

    // generate jwt
    $jwt = JWT::encode($token, $key);
    echo json_encode(
        array(
            "message" => "Uspješan login.",
            "jwt" => $jwt
        )
    );
}else {
    http_response_code(401);

    // tell the user login failed
    echo json_encode(array("message" => "Login failed."));
}
