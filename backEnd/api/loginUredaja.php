<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

include_once '../../baza/Database.php';
include_once '../models/Uredaj.php';

$database = new Database();
$db = $database->connect();

$uredaj = new Uredaj($db);
$data = json_decode(file_get_contents("php://input"));

$uredaj->kod = $data->kod;
$uredaj_postoji = $uredaj->kodPostoji();

include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

if($uredaj_postoji){
    $token = array(
        "iss" => $iss,
        "aud" => $aud,
        "iat" => $iat,
        "nbf" => $nbf,
        "exp" => $exp,
        "data" => array(
            "kod" => $uredaj->kod
        )
    );

    http_response_code(200);
    http_response_code(200);

    // generate jwt
    $jwt = JWT::encode($token, $key);
    echo json_encode(
        array(
            "message" => "Uspješan login.",
            "jwt" => $jwt
        )
    );
}else {
    http_response_code(401);

    // tell the user login failed
    echo json_encode(array("message" => "Login nije uspio."));
}
