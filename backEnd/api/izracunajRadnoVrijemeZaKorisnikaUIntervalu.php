<?php


header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

include_once '../../baza/Database.php';
include_once '../models/Tajnica.php';
include_once '../models/Korisnik.php';
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
$database = new Database();
$db = $database->connect();

$tajnica = new Tajnica($db);
$korisnik = new Korisnik($db);
$data = json_decode(file_get_contents("php://input"));
$jwt=isset($data->jwt) ? $data->jwt : "";
if($jwt) {

    // if decode succeed
    try {

        // decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $korisnik->ID = $data->ID;
        $datumOd = $data->datumOd;
        $datumDo = $data->datumDo;
        $result = $tajnica->izracunajRadnoVrijemeZaKorisnikaUIntervalu($korisnik, $datumOd, $datumDo);
        if($result == -1){
            echo json_encode(array("message" => "DatumOd treba biti manji ili jednak datumDo"));
        } else {
            $arr = array('brojSati' => $result);
            echo json_encode($arr);
        }
    } catch (Exception $e) {

        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array(
            "message" => "Pristup odbijen.",
            "error" => $e->getMessage()
        ));
    }
}