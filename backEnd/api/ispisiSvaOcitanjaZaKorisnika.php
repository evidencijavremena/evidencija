<?php


header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");

include_once '../../baza/Database.php';
include_once '../models/Tajnica.php';
include_once '../models/Korisnik.php';
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
$database = new Database();
$db = $database->connect();

$tajnica = new Tajnica($db);
$data = json_decode(file_get_contents("php://input"));
$idFind = $data->idKorisnika;
$jwt=isset($data->jwt) ? $data->jwt : "";
if($jwt) {

    // if decode succeed
    try {

        // decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $result = $tajnica->ispisiSvaOcitanjaZaKorisnika($idFind);
        $num = $result->rowCount();

        if($num>0) {
            $arr = array();
            $arr['data'] = array();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                extract($row);

                $item = array(
                    'ID' => $ID,
                    'ime' => $ime,
                    'prezime' => $prezime,
                    'pozicija' => $pozicija,
                    'vrijeme' => $vrijeme,
                    'razlog' => $razlog,
                    'lokacija' => $lokacija,
                    'uredaj' => $uredajNaziv,
                    'automobil' => $automobilNaziv
                );

                array_push($arr['data'], $item);
            }

            echo json_encode($arr);
        } else {
            echo json_encode(
                array('message' => 'Ništa nije pronađeno.')
            );
        }
    }catch (Exception $e) {

        // set response code
        http_response_code(401);

        // show error message
        echo json_encode(array(
            "message" => "Pristup odbijen.",
            "error" => $e->getMessage()
        ));
    }
}