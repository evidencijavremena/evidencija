CREATE TABLE admin(
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL
);
CREATE TABLE korisnik (
    id int(11) NOT NULL AUTO_INCREMENT,
    ime varchar(255) NOT NULL,
    prezime varchar(255) NOT NULL,
    datumRodenja datetime NOT NULL,
    pozicija varchar(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE  karticaKorisnik (
    NFCID int(11) NOT NULL AUTO_INCREMENT,
    ID_korisnik int(11) NOT NULL,
    PRIMARY KEY(NFCID),
    FOREIGN KEY(ID_korisnik) REFERENCES korisnik(id)
);

CREATE TABLE razlog (
    ID int(11) NOT NULL AUTO_INCREMENT,
    razlog varchar (255) NOT NULL,
    PRIMARY KEY(ID)
);

CREATE TABLE  uredaj (
    ID int(11) NOT NULL AUTO_INCREMENT,
    naziv varchar(255) NOT NULL,
    kod varchar(255) NOT NULL,
    PRIMARY KEY(ID)
);

CREATE TABLE automobil (
    ID int(11) NOT NULL AUTO_INCREMENT,
    naziv varchar(255) NOT NULL,
    PRIMARY KEY(ID)
);

CREATE TABLE  ocitanje (
    ID int(11) NOT NULL AUTO_INCREMENT,
    ID_korisnik int(11) NOT NULL,
    vrijeme datetime NOT NULL,
    lokacija varchar(255)  NOT NULL,
    ID_razlog int(11) NOT NULL,
    ID_uredaj int(11) NOT NULL,
    ID_automobil int(11) NOT NULL,
    PRIMARY KEY(ID),
    FOREIGN KEY(ID_korisnik) REFERENCES korisnik(id),
    FOREIGN KEY(ID_razlog) REFERENCES razlog(ID),
    FOREIGN KEY(ID_uredaj) REFERENCES uredaj(ID),
    FOREIGN KEY(ID_automobil) REFERENCES automobil(ID)
);

INSERT INTO admin(username,password) VALUES('tajnica', '$2y$12$sXwH8gHOmg9Y/NPDDkXOVeaWQ/TDPLIIUmUnQRJ3GmVzIdmmFM0hu');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('John', 'Doe', '1998-12-01 10:00:00', 'volonter');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('Michael', 'Doe', '1998-10-02 10:00:00', 'volonter');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('James', 'Loe', '1998-11-03 10:00:00', 'zaposlenik');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('Charles', 'Loe', '1998-01-04 10:00:00', 'zaposlenik');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('John', 'Poe', '1998-01-05 10:00:00', 'zaposlenik');
INSERT INTO karticaKorisnik(ID_korisnik) VALUES(1);
INSERT INTO karticaKorisnik(ID_korisnik) VALUES(2);
INSERT INTO karticaKorisnik(ID_korisnik) VALUES(3);
INSERT INTO karticaKorisnik(ID_korisnik) VALUES(4);
INSERT INTO karticaKorisnik(ID_korisnik) VALUES(5);
INSERT INTO razlog(razlog) VALUE('DOLAZAK');
INSERT INTO razlog(razlog) VALUE('PAUZA');
INSERT INTO razlog(razlog) VALUE('ODLAZAK');
INSERT INTO uredaj(naziv, kod) VALUES('Zagreb-sjever', 'kod123');
INSERT INTO uredaj(naziv, kod) VALUES('Zagreb-jug', 'kod234');
INSERT INTO automobil(naziv) VALUE ('Audi');
INSERT INTO automobil(naziv) VALUE ('Mercedes');
INSERT INTO automobil(naziv) VALUE ('BMW');
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-11-01 10:00:00', 'Zagreb', 1, 1, 1);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-11-01 12:00:00', 'Zagreb', 2, 1, 1);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-11-01 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-12-15 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-12-15 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (1, '2019-12-15 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2019-11-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2019-11-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2019-11-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2020-01-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2020-01-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (2, '2020-01-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-11-15 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-11-15 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-11-15 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-12-28 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-12-28 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (3, '2019-12-28 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2019-12-08 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2019-12-08 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2019-12-08 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2020-01-05 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2020-01-05 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (4, '2020-01-05 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-20 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-20 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-20 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2020-01-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2020-01-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2020-01-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-21 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-21 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2019-12-21 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2020-01-04 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(ID_korisnik, vrijeme, lokacija, ID_razlog, ID_uredaj, ID_automobil) VALUES (5, '2020-01-04 18:00:00', 'Zagreb', 2, 2, 2);




