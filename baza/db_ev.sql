CREATE TABLE admin(
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL
);
CREATE TABLE korisnik (
    ID int(11) NOT NULL AUTO_INCREMENT,
    ime varchar(255) NOT NULL,
    prezime varchar(255) NOT NULL,
    datumRodenja varchar(255) NOT NULL,
    pozicija varchar(255) NOT NULL,
    aktivan boolean NOT NULL DEFAULT true,
    PRIMARY KEY(ID)
);

CREATE TABLE  karticaKorisnik (
    NFC_ID varchar(255) NOT NULL,
    korisnik_ID int(11) NOT NULL,
    aktivan boolean NOT NULL DEFAULT true,
    PRIMARY KEY(NFC_ID),
    FOREIGN KEY(korisnik_ID) REFERENCES korisnik(ID)
);

CREATE TABLE razlog (
    ID int(11) NOT NULL AUTO_INCREMENT,
    razlog varchar (255) NOT NULL,
    PRIMARY KEY(ID)
);

CREATE TABLE  uredaj (
    ID int(11) NOT NULL AUTO_INCREMENT,
    naziv varchar(255) NOT NULL UNIQUE,
    kod varchar(255) NOT NULL UNIQUE,
    aktivan boolean NOT NULL DEFAULT true,
    PRIMARY KEY(ID)
);

CREATE TABLE automobil (
    ID int(11) NOT NULL AUTO_INCREMENT,
    naziv varchar(255) NOT NULL,
    aktivan boolean NOT NULL DEFAULT true,
    PRIMARY KEY(ID)
);

CREATE TABLE  ocitanje (
    ID int(11) NOT NULL AUTO_INCREMENT,
    korisnik_ID int(11) NOT NULL,
    vrijeme datetime NOT NULL,
    lokacija varchar(255)  NOT NULL,
    razlog_ID int(11) NOT NULL,
    uredaj_ID int(11) NOT NULL,
    automobil_ID int(11),
    PRIMARY KEY(ID),
    FOREIGN KEY(korisnik_ID) REFERENCES korisnik(ID),
    FOREIGN KEY(razlog_ID) REFERENCES razlog(ID),
    FOREIGN KEY(uredaj_ID) REFERENCES uredaj(ID),
    FOREIGN KEY(automobil_ID) REFERENCES automobil(ID)
);

INSERT INTO admin(username, password) VALUES('tajnica', '$2y$12$sXwH8gHOmg9Y/NPDDkXOVeaWQ/TDPLIIUmUnQRJ3GmVzIdmmFM0hu');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('John', 'Doe', '1998-12-01 10:00:00', 'volonter');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('Michael', 'Doe', '1998-10-02 10:00:00', 'volonter');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('James', 'Loe', '1998-11-03 10:00:00', 'zaposlenik');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('Charles', 'Loe', '1998-01-04 10:00:00', 'zaposlenik');
INSERT INTO korisnik(ime, prezime, datumRodenja, pozicija) VALUES('John', 'Poe', '1998-01-05 10:00:00', 'zaposlenik');
INSERT INTO razlog(razlog) VALUE('DOLAZAK');
INSERT INTO razlog(razlog) VALUE('PAUZA');
INSERT INTO razlog(razlog) VALUE('ODLAZAK');
INSERT INTO razlog(razlog) VALUE('POSLOVNI DOLAZAK');
INSERT INTO razlog(razlog) VALUE('POSLOVNI IZLAZAK');
INSERT INTO razlog(razlog) VALUE('PRIVATNI IZLAZAK');
INSERT INTO uredaj(naziv, kod) VALUES('Zagreb-sjever', 'kod123');
INSERT INTO uredaj(naziv, kod) VALUES('Zagreb-jug', 'kod234');
INSERT INTO automobil(naziv) VALUE ('Audi');
INSERT INTO automobil(naziv) VALUE ('Mercedes');
INSERT INTO automobil(naziv) VALUE ('BMW');
INSERT INTO karticaKorisnik(NFC_ID,korisnik_ID)  VALUEs ('EF3C4C0D', 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2018-11-01 08:00:00', 'Zagreb', 1, 1, 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2018-11-01 10:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2018-11-01 22:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 08:00:00', 'Zagreb', 1, 1, 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 09:00:00', 'Zagreb', 2, 1, 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 10:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 12:00:00', 'Zagreb', 1, 1, 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 13:00:00', 'Zagreb', 2, 1, 1);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-11-01 14:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-12-15 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-12-15 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (1, '2019-12-15 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2019-11-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2019-11-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2019-11-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2020-01-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2020-01-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (2, '2020-01-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-11-15 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-11-15 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-11-15 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-12-28 08:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-12-28 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (3, '2019-12-28 16:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2019-12-08 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2019-12-08 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2019-12-08 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2020-01-05 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2020-01-05 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (4, '2020-01-05 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-20 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-20 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-20 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2020-01-02 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2020-01-02 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2020-01-02 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-21 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-21 12:00:00', 'Zagreb', 2, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2019-12-21 18:00:00', 'Zagreb', 3, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2020-01-04 10:00:00', 'Zagreb', 1, 2, 2);
INSERT INTO ocitanje(korisnik_ID, vrijeme, lokacija, razlog_ID, uredaj_ID, automobil_ID) VALUES (5, '2020-01-04 18:00:00', 'Zagreb', 2, 2, 2);




